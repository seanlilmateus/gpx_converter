package de.project.mateus.gpsconverter

const val ORIGINAL_FORMAT = "yyMMddhhmmss"
const val HEADER_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
const val OUTPUT_DATE = "yyyy-MM-dd'T'HH:mm:ss'Z'"


const val COLUMBUS_V900 = "INDEX,TAG,DATE,TIME,LATITUDE N/S,LONGITUDE E/W,HEIGHT,SPEED,HEADING,VOX"


enum class FileType {
  V900, Unknown
}

fun fileTypeFor(value: String) = when(value) {
  COLUMBUS_V900 -> FileType.V900
  else          -> FileType.Unknown
}

sealed class Location {
    abstract val elevation: String
    abstract val lat: String
    abstract val lon: String
    abstract val time: String
}

data class Course(val speed: String
                  , val course: String
                  , override val elevation: String
                  , override val lat: String
                  , override val lon: String
                  , override val time: String) : Location() {
    override fun toString():String = courseString(this)
}

data class Position(val name: String, val cmt: String, val desc: String
                    , override val elevation: String
                    , override val lat: String
                    , override val lon: String
                    , override val time: String) : Location() {
    override fun toString():String = positionString(this)
}

fun composeXML(createdAt: String, minlat: String, maxlat: String, minlon:String, maxlon: String, locations:String, courses: String) = """<?xml version="1.0" encoding="UTF-8"?>
<gpx version="1.0" creator="GPS Converter - https://gitlab.com/seanlilmateus/gpx_converter" xmlns="http://www.topografix.com/GPX/1/0">
  <time>${createdAt}</time>
  <bounds minlat="$minlat" minlon="$minlon" maxlat="$maxlat" maxlon="$maxlon"/>${locations}
  <trk>
    <name>V900 tracklog</name>
    <desc>V900 GPS tracklog data</desc>
    <trkseg>${courses}
    </trkseg>
  </trk>
</gpx>
"""

private fun courseString(it: Course): String = """
      <trkpt lat="${it.lat}" lon="${it.lon}">
        <ele>${it.elevation}</ele>
        <time>${it.time}</time>
        <course>${it.speed}</course>
        <speed>${it.speed}</speed>
      </trkpt>"""

private fun positionString(it: Position): String = """
  <wpt lat="${it.lat}" lon="${it.lon}">
    <ele>${it.elevation}</ele>
    <time>${it.time}</time>
    <name>${it.desc}</name>
    <cmt>${it.desc}</cmt>
    <desc>${it.desc}</desc>
  </wpt>"""


private fun buildCourse(data:Map<String, String>): Course {
    val speed = data["speed"] ?: "0"
    val course = data["course"] ?: "UNKOWN"
    val elevation = data["elevation"] ?: "0"
    val lat = data["lat"] ?: "0"
    val lon = data["lon"] ?: "0"
    val time = data["date"] ?: "0"
    return Course(speed, course, elevation, lat, lon, time)
}


private fun buildPosition(data: Map<String, String>): Position {
    val name = data["index"] ?: "FAILED"
    val cmt = data["index"] ?: "FAILED"
    val desc = data["index"] ?: "FAILED"
    val elevation = data["elevation"]  ?: "0"
    val lat = data["lat"] ?: "0"
    val lon = data["lon"] ?: "0"
    val time = data["date"] ?: "0"
    return Position(name, cmt, desc, elevation, lat, lon, time)
}

// T - Trackpoint, C-Waypoint, V-Voicetagged point
// - "T" : normal point
// - "C" : marked waypoint (when POI button is pressed)
// - "V" : marked waypoint with voicerecord, in this case,
//         the filename of the voice record (without the ".wav" extension) is in the VOX field
// - "G" : tag appears to be a 'T' tag, but generated on the trailing
// - "P" : für ein POI mit Photoinfo
fun parserCourse(data: Map<String, String>) = buildCourse(data) // normal point

fun parserWaypoint(data: Map<String, String>) = buildPosition(data) // everything else is tagged POI

fun parser(data: Map<String, String>) = when(data["type"]){
// "C", "G", "V" -> buildPosition(data)
    "T" -> buildCourse(data) // normal point
    else -> buildPosition(data) // everything else is tagged POI
}

