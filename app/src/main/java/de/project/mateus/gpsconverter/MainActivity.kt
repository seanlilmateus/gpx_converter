package de.project.mateus.gpsconverter

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.View
import android.widget.Button
import java.io.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*
import java.util.stream.Collectors
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    companion object {
        const val REQUEST_PERMISSION = 1
        const val ACTIVITY_CHOOSE_FILE = 123
        const val TAG = "MainActivity"
        val NotLetter: (Char) -> Boolean = { value -> !value.isLetter() }

        val outFormatter =  SimpleDateFormat(OUTPUT_DATE, Locale.GERMANY)
        val formatter = SimpleDateFormat(ORIGINAL_FORMAT, Locale.GERMANY)
        val headerFormatter = SimpleDateFormat(HEADER_DATE, Locale.GERMANY)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressBar.visibility = View.GONE
        val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION)
        }
        if (isExternalStorageReadable()) {
            open_button.setOnClickListener {
                onBrowse()
            }
        } else {
            longSnackbar(open_button, "SDCard Not Found")
            open_button.enabled = false
        }
    }

    fun onBrowse() {
        val chooseFile = Intent(Intent.ACTION_GET_CONTENT)
        chooseFile.addCategory(Intent.CATEGORY_OPENABLE)
        chooseFile.type = "text/*"
        intent = Intent.createChooser(chooseFile, "Choose a File to Convert")
        startActivityForResult(intent, ACTIVITY_CHOOSE_FILE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent) {
        super.onActivityResult(
                requestCode,
                resultCode,
                intent
        )
        if (requestCode == ACTIVITY_CHOOSE_FILE && resultCode == RESULT_OK) {
            val uri = intent.data
            val path = uri.path.split(":|/|\\.".toRegex())
            val destFilename = path[path.lastIndex - 1]
            progressBar.visibility = View.VISIBLE
            doAsync {
                realFileContent(uri)?.let { content ->
                    val (marked, positions) = content.partition { it is Position }
                    val composedXML  = composeXML(
                            createdAt = headerFormatter.format(Date()),
                            minlat = content.minBy { it.lat.toFloat() }!!.lat,
                            maxlat = content.maxBy { it.lat.toFloat() }!!.lat,
                            minlon = content.minBy { it.lon.toFloat() }!!.lon,
                            maxlon = content.maxBy { it.lon.toFloat() }!!.lon,
                            locations = marked.joinToString(""),
                            courses = positions.joinToString("")
                    )
                    writeFile(destFilename, composedXML)
                    uiThread { mainActivity ->
                        progressBar.visibility = View.GONE
                        val message = "File: " + destFilename + ".gpx has been written in the SDCard"
                        longSnackbar(open_button, message, "Open File") {
                            val openIntent = Intent(Intent.ACTION_VIEW)
                            val filePath = "${Environment.getExternalStorageDirectory()}/$destFilename.gpx"
                            openIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            val file = File(filePath)
                            val fileUri = FileProvider.getUriForFile(mainActivity,  "${applicationContext.packageName}.provider", file)
                            openIntent.setDataAndType(fileUri, "application/gpx+xml")
                            startActivity(openIntent)
                        }
                    }
                } ?: uiThread {
                  progressBar.visibility = View.GONE
                  longSnackbar(open_button, "Invalide File Format") 
                }

            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_PERMISSION -> if (!(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                longToast("Permission not Granted")
                this.finish()
            }
        }
    }

    private fun isValid(type: String) = fileTypeFor(value = type) != FileType.Unknown

    private fun transformToDoublePair(lat:String, lon:String) = Pair(
            lat.dropLast(1).toDouble(),
            lon.dropLast(1).toDouble()
    )

    /*
    + + : North and East
    + - : North and West
    - - : South and West
    - + : South and East
    * */
    private fun composeLatLon(lat:String, lon:String): Pair<Double, Double> {
        val pair = Pair(lat.dropWhile(NotLetter), lon.dropWhile(NotLetter))
         return when(pair) {
            // Pair("N", "E") -> transform(lat, lon)
            Pair("N", "W") -> transformToDoublePair(lat, lon).mapS(Double::unaryMinus)
            Pair("S", "W") -> transformToDoublePair(lat, lon).map(Double::unaryMinus)
            Pair("S", "E") -> transformToDoublePair(lat, lon).mapF(Double::unaryMinus)
            else -> transformToDoublePair(lat, lon)
        }
    }

    private fun realFileContent(uri: Uri): List<Location>? {
        var index = 0
        val specialTags = listOf("C", "G")
        val inputStream = contentResolver.openInputStream(uri)
        val reader = BufferedReader(InputStreamReader(inputStream, "UTF-8"))
        var waypoints = mutableListOf<Position>()
        if (!isValid(type = reader.readLine())) { return null }

        val retValue = reader.lines().map { oline ->
            val line = oline.replace("\\p{C}".toRegex(), "")
            val fields = line.split(',')
            val (lat, lon) = composeLatLon(fields[4], fields[5])
            val elevation = fields[6].toDouble()
            val date = outFormatter.format(formatter.parse(fields[2] + fields[3]))
            val course = fields[8].toDouble()
            val speed: Double = if (fields[1] in specialTags) {
                index += 1
                0.0
            } else {
                fields[7].toInt().toDouble() / 3.6
            }
            val pairs = mapOf(
                    "type"      to fields[1],
                    "lat"       to "%.09f".format(Locale.ROOT, lat),
                    "lon"       to "%.09f".format(Locale.ROOT, lon),
                    "date"      to date,
                    "course"    to "%.6f".format(Locale.ROOT, course),
                    "elevation" to "%.6f".format(Locale.ROOT, elevation),
                    "index"     to "WPT%03d".format(index),
                    "speed"     to "%.6f".format(Locale.ROOT, speed)
            )
            if (fields[1] in specialTags) { waypoints.add(parserWaypoint(pairs)) }
            parserCourse(pairs)
        }.collect(Collectors.toList()) + waypoints
        reader.close()
        return retValue
    }



    private fun isExternalStorageWritable(): Boolean{
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }


    fun writeFile(filename: String, content: String) {
        if (isExternalStorageWritable()){
            val sdcard = Environment.getExternalStorageDirectory()
            val dir = File(sdcard.getAbsolutePath())
            dir.mkdir()
            val file = File(dir, "${filename}.gpx")
            val os = FileOutputStream(file)
            os.write(content.toByteArray())
            os.close()
        }
    }


    /* Checks if external storage is available to at least read */
    private fun isExternalStorageReadable(): Boolean {
        return Environment.getExternalStorageState() in
                setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
    }
}

private var Button.enabled: Boolean
    get() = this.isEnabled
    set(value) { this.setEnabled(value) }

private fun <A, R>  Pair<A, A>.map(f1:((A)-> R)) = Pair(f1(this.first), f1(this.second))
private fun <A, B, R>  Pair<A, B>.mapF(f1:((A)-> R)) = Pair(f1(this.first), this.second)
private fun <A, B, R>  Pair<A, B>.mapS(f1:((B)-> R)) = Pair(this.first, f1(this.second))
